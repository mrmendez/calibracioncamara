**ALGORITMOS PARA CALIBRACIÓN DE LA LENTE DE LA CÁMARA**

Puede encontra más información respecto a este algoritmo en:
https://www.academia.edu/76369062/Calibracion_Camara 

Se recomienda instalar todas las bibliotecas necesarias antes de ejecutar el código.
---

## Comandos básicos en S.O. LINUX

Algunos comandos que le ayudarán a configurar su equipo.

1. pkg-config --modversion opencv  <== le indica la versión de OpenCV que tiene instalada.
2. opencv_version <== Otro comando para conocer la versión de OpenCV.
3. find . | grep opencv  <== le ayuda a ubicar en donde están los archivos de opencv.

Una ruta particular puede ser:
/usr/lib/x86_64-linux-gnu/  (Ej: libopencv_core.so)

Si usa CodeBlock IDE debe ir a configuraciones y en el Linker del compilador agregar las que se ocupen:
<opencv2/core/core.hpp>
<opencv2/imgproc/imgproc.hpp>
<opencv2/highgui/highgui.hpp>
<opencv2/calib3d.hpp>
<opencv2/video.hpp>
---

## Como usarlo

Debe tener impresa la imagen de un tablero de ajedrez y verificar que este en una superficie lisa y rígida su imagen.

Al momento de ejecutarlo deberá indicarle referente a la imagen lo siguiente:

1. Número de esquinas a lo ancho.
2. Número de esquinas a lo alto.
3. Número de imágenes a analizar.

---
